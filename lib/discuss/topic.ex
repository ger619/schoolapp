defmodule Discuss.Topic do
  use Ecto.Schema
  import Ecto.Changeset
  alias Discuss.User
  alias Discuss.Comment

  schema "topics" do
    field :title, :string
    belongs_to :user, User
    has_many :comments, Comment
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title, :user_id])
    |> validate_required([:title])
  end
end
