defmodule Discuss.Comment do
  use Ecto.Schema
  import Ecto.Changeset
  alias Discuss.Topic
  alias Discuss.User

  @derive {Jason.Encoder, only: [:content]}

  schema "comments" do
    field :content, :string
    belongs_to :user, User
    belongs_to :topic, Topic

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:content, :user_id, :topic_id])
    |> validate_required([:content])
  end
end
